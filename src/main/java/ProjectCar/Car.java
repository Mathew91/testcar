package ProjectCar;

public class Car {
    int maxSpped;
    String Brand;

    public Car(int maxSpped, String brand) {
        this.maxSpped = maxSpped;
        Brand = brand;
    }

    public void printCar(){
        System.out.println("Auto marki : " + Brand + "osiaga predkość : " + maxSpped);
    }

    public void setMaxSpped(int maxSpped) {
        this.maxSpped = maxSpped;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }
}
